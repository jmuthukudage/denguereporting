@extends('main')

@section('title', 'Case Reporting')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
            <form method="POST" action="save_cases">
                {{ csrf_field() }}
                <label>Cases <input type="number" id="cases" name="cases"></label>
                <label>District @include('districts')</label>
                <label>Date <input type="date" id = "case-date" name="case-date"></label>
                <button id="submit" type="submit">Save</button>


            </form>
@endsection

           
