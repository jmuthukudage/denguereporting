@extends('main')

@section('title', 'Case Reporting')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection
<form method="get" action="getdata">

    {{ csrf_field() }}
    <label>District @include('districts')</label>
    <label>Year @include('year')</label>
    <label>Month @include('month')</label>
    <label>Week @include('week')</label>
    <button id="retrieve" name="retrieve" type="button">Retrieve</button>

</form>

<div class="col-12">
	<div class="col-6" id="chart">
	</div>
	<div id ="container" class="col-6">
	</div>

</div>
