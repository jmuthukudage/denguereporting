<html>
    <head>
        <title>@yield('title')</title>
        <script type="text/javascript" src="{{URL::asset('js/jquery3.1.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/tether.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/main.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/datatables.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/highcharts.src.js')}}"></script>

        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/datatables.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap/bootstrap.min.css')}}">
    </head>
    <body>
        @section('sidebar')
            master sidebar.
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>