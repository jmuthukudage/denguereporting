<?php

namespace App\Http\Controllers;

use App\DenguecaseUser;
use Input;
use Illuminate\Http\Request;

class CaseReportingController extends Controller
{
    public function index()
    {
       return view('case_reporting');
    }

    public function create()
    {
    	$case = new DenguecaseUser;

    	 if(Input::has('district'))
    	 {
    	 	$case->district = Input::get('district');
    	 }

    	 if(Input::has('case-date'))
    	 {
            
            $date = Input::get('case-date');
            $time = strtotime( $date);
            $date = date('Y-m-d', $time);
           	$case->year = date('Y',$time);
            $case->month = date('m',$time);
            $case->week = date('W',$time);
    	 }

    	 if(Input::has('cases'))
    	 {
    	 	$case->cases = Input::get('cases');
    	 }
         $case->save();
         return redirect('/case_reporting');
    }

    
}
