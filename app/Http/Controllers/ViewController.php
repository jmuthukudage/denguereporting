<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Denguecase;
use \Input;

class ViewController extends Controller
{
    function index()
    {
    	return \View::make('queries/index');
    }

    public function view()
    {

        $cases = Denguecase::orderBy('id');

        if(Input::get('district') != '0')
        {
        	 $cases->where('district', '=', Input::get('district'));
        }
        if(Input::get('month') != '0')
        {
        	 $cases->where('month', '=', Input::get('month'));
        }
        if(Input::get('week') != '0')
        {
        	 $cases->where('week', '=', Input::get('week'));
        }
        if(Input::get('year') != '0')
        {
        	 $cases->where('year', '=', Input::get('year'));
        }
        $results = $cases->get();

        if(Input::has('type'))
        {
          $type = Input::get('type');

          switch ($type) {
          	case 'json':
          		return $this->prepareForCharts($results);
          		break;
          	
          	default:
          		# code...
          		break;
          }
        }

        return \View::make('queries.case_view',['data'=>$results]);

    }

    public function prepareForCharts($data)
    {
    	$chartData = array();

    	foreach ($data as $item) {
    		if(array_key_exists($item->district, $chartData))
    		{
    			$chartData[$item->district] += $item->cases;
    		}
    		else
    		{
    			$chartData[$item->district] = $item->cases;
    		}
    	}

    	$categories = array_keys($chartData);
    	$series = array_values($chartData);

    	return ['categories'=>$categories, 'series'=>$series];
    }
}
