<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denguecase extends Model
{
    protected $table = 'cases';

    protected $fillable = ['year', 'month', 'week', 'district', 'incidence', 'rain', 'temp',
     'pop_den', 'pop', 'cases', 'village', 'ndvi'];
}
