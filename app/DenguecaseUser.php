<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DenguecaseUser extends Model
{
    protected $table = 'cases_user';

    protected $fillable = ['year', 'month', 'week', 'district', 'incidence', 'rain', 'temp',
     'pop_den', 'pop', 'cases', 'village', 'ndvi'];
}
