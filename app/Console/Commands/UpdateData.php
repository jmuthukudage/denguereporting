<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Denguecase;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateData:data {fileName} {year} {dataType}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update data into table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   $dataFile = $this->argument('fileName');
        $year = $this->argument('year');
        $dataType = $this->argument('dataType'); //temp, popden, rainfall, case
   
        if(isset($dataFile) and isset($year) && isset($dataType))
        {

            //loading data from csv file
            $fileToRead = storage_path().'/dataFiles/'.$dataFile;
            $row = 0;
            $firstRow = array();
            if (($handle = fopen($fileToRead, "r")) !== FALSE) 
            {
                while (($data = fgetcsv($handle)) !== FALSE) {
                 $num = count($data);
                    if($row ==0)
                    {
                       $firstRow = $data;
                       $row++;
                       print_r($firstRow);
                       continue;
                    }

                    for($i=1;$i<$num;$i++)
                    {
                     
                      $case = Denguecase::firstOrNew(['district'=> $data[0], 'year'=> $year, 'week'=>$firstRow[$i]]);

                      if(isset($case->year))
                      {   
                          $case->year = $year;
                          $case->week = $firstRow[$i];
                          $case->district = $data[0];
                          
                          switch ($dataType) 
                          {
                              case 'temp':
                                  $case->temp = $data[$i];
                                  break;
                              case 'pop':
                                  $case->pop = $data[$i];
                                  break;  
                              case 'rain':
                                  $case->rain = $data[$i];
                                  break;  
                              case 'popden':
                                  $case->pop_den = $data[$i];
                                  break; 
                              case 'case':
                                  $case->cases = $data[$i];
                                  break;
                              case 'ndvi':
                                  $case->ndvi = $data[$i];
                                  break;                             
                              default:
                                  # code...
                                  break;
                          }
                         // echo $case->temp . " " .$case->ndvi.PHP_EOL;
                          $case->save();
                       }
                    }
                    $row++;                    
                    }
                fclose($handle);
           }

            
        }
        else
        {
            echo "Failed: usage updateData:data {fileName} {year} {dataType}";
        }


    }
}

