<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Denguecase;

class loadData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loadData:temp {fileName} {year}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load temp data into table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataFile = $this->argument('fileName');
        $year = $this->argument('year');


        if(isset($dataFile) and isset($year))
        {

            //loading data from csv file
            $fileToRead = storage_path().'/dataFiles/'.$dataFile;
            $row = 0;
            $firstRow = array();
            if (($handle = fopen($fileToRead, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
             $num = count($data);
                if($row ==0)
                {
                   $firstRow = $data;
                   $row++;
                   continue;
                }

                for($i=1;$i<$num;$i++)
                {
                  $case = new Denguecase;
                 
                  $case->year = $year;
                  $case->week = $firstRow[$i];
                  $case->district = $data[0];
                  $case->temp = $data[$i];

                  $case->save();

                }

                

                $row++;
                
                }
            fclose($handle);
        }

            
        }


    }
}
