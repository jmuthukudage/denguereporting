$(document).ready(function(){

	$('#retrieve').click(function(){

        year = $('#year').val();
        month = $('#month').val();
        week = $('#week').val();
        district = $('#district').val();

        
		$.ajax({
			method: "GET",
			url: "getdata",
			data: { district: district, year: year, month: month, week: week}
		})
		.done(function( msg ) {
			$('#container').html(msg);
			$('#case-view').DataTable({});
		});


		$.ajax({
			method: "GET",
			url: "getdata",
			data: { district: district, year: year, month: month, week: week, type: 'json'}
		})
		.done(function( msg ) {
			console.log(msg);
			buildCharts(msg);
		});

      
	});
   
  function buildCharts(msg)
  {

  	 $('#chart').highcharts({ // starting highchart
                            chart: {
                                type: 'line',
                                height:300,
                                width: 800
                            },
                            plotOptions:{
                               series:
                               {
                                pointWidth: 20,
                                shadow: true,
                               },
                               dataLabels : {
                                style: {
                                    width: '100px',
                                    fontSize: '6px'
                                }
                               }
                            },
                            title:
                            {
                                text: ''
                            },
                            xAxis:{
                                categories: msg['categories'],
                               

                            },
                            yAxis:{
                                title: { text: 'Cases'},
                                //alternateGridColor: '#FDFFD5',
                                tickInterval: 20,

                            },
                            series: [{name: 'cases', data:msg['series']}],
                            credits:
                            {
                                enabled: false
                            },
                            legend: {
                               enabled: false,
                            }
                         }); // end of high chart

  }

});