
-- Table: cases

-- DROP TABLE cases;

CREATE TABLE cases
(
  id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  year integer,
  month integer,
  week integer,
  district character varying,
  incidence double precision,
  rain double precision,
  temp double precision,
  pop_den double precision,
  pop double precision,
  cases double precision,
  village character varying(20),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT "primary" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cases
  OWNER TO postgres;